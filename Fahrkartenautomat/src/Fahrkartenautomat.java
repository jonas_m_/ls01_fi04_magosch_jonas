﻿import java.util.Scanner;

class Fahrkartenautomat {

//	-----------------------------------------------------------------------------------------------------------
//	Main - METHODE:

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfenesGeld;
		double rückgabebetrag;
		int wiederholung = 0;

//		Ablauf - SCHLEIFE:

		do {
			zuZahlenderBetrag = farkartenBestellungErfassen();

			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("Möchten Sie einen weiteren Kaufvorgang tätigen? \n");

			System.out.println("1. Ja");
			System.out.println("2. Nein");

			wiederholung = tastatur.nextInt();

			System.out.println("");

		} while (wiederholung == 1);

		System.out.println("Vielen Dank für Ihren Einkauf!\n" + "Wir wünschen Ihnen eine gute Fahrt.\n");
	}

//	-----------------------------------------------------------------------------------------------------------
//	Fahrkartenbestellung erfassen - METHODE:

	public static double farkartenBestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		int wahl;

//		Fahrkartenbezeichnung - ARRAY:

		String[] fahrkartenBezeichnung = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

//		Fahrkartenpreise - ARRAY:

		double[] preise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

//		Ticketauswahl - SCHLEIFE:

		do {
			System.out.println("Bitte wählen Sie Ihr Ticket: \n");

			int länge = 9;

			if (fahrkartenBezeichnung.length < 10) {
				länge = fahrkartenBezeichnung.length;
			}

			for (int i = 0; i < länge; i++) {

				System.out.printf(" " + (i + 1) + ". " + fahrkartenBezeichnung[i] + " [%.2f €", preise[i]);
				System.out.print("] \n");
			}

			if (fahrkartenBezeichnung.length >= 10) {
				for (int i = 9; i < fahrkartenBezeichnung.length; i++) {

					System.out.printf((i + 1) + ". " + fahrkartenBezeichnung[i] + " [%.2f €", preise[i]);
					System.out.print("] \n");
				}
			}

			System.out.println("");
			System.out.print("Eingabe: ");
			wahl = tastatur.nextInt();

			if (wahl > fahrkartenBezeichnung.length || wahl < 1) {
				System.out.println("");
				System.out.println("Falsche Eingabe! Bitte wiederholen. \n");
				System.out.print("Eingabe: ");
			}

		} while (wahl > fahrkartenBezeichnung.length || wahl < 1);

		zuZahlenderBetrag = preise[wahl - 1];

		int anzahlTicket;

//		Ticketanzahl wählen - ABFRAGE:

		System.out.print("Anzahl der Tickets (1-10 möglich): ");
		anzahlTicket = tastatur.nextInt();

		while (anzahlTicket > 10 || anzahlTicket < 1) {
			System.out.println("");
			System.out.println("Falsche Eingabe! Bitte wiederholen. \n");
			System.out.print("Eingabe: ");
			anzahlTicket = tastatur.nextInt();

		}

//		Rechne Ticketanzahl * Preis - OPERATION:

		zuZahlenderBetrag = zuZahlenderBetrag * anzahlTicket;

		return zuZahlenderBetrag;

	}

//	-----------------------------------------------------------------------------------------------------------
//	Fahrkarten bezahlen - METHODE:

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		int Zahlungsmethode;
		double ausstehenderBetrag = zuZahlenderBetrag;
		System.out.println("");
		System.out.printf("Zu zahlen: %.2f €\n", ausstehenderBetrag);
		System.out.println("");
		System.out.println("Möchten Sie Bar oder mit Karte zahlen?");
		System.out.println("1. Bar");
		System.out.println("2. Karte");
		Zahlungsmethode = tastatur.nextInt();

		while (Zahlungsmethode != 1 & Zahlungsmethode != 2) {
			System.out.println("");
			System.out.println("Falsche Eingabe! Bitte wiederholen. \n");
			System.out.print("Eingabe: ");
			Zahlungsmethode = tastatur.nextInt();
		}

		if (Zahlungsmethode == 2) {

			System.out.println("");
			System.out.println("Bitte halten Sie Ihre Karte an das Lesegerät");

			for (int i = 0; i < 15; i++) {
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			System.out.println("");
			System.out.println("Karte wird gelesen");

			for (int i = 0; i < 4; i++) {
				System.out.print("-");
				try {
					Thread.sleep(150);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			System.out.println("");
			System.out.println("Zahlung wird verarbeitet");

			for (int i = 0; i < 15; i++) {
				System.out.print("=");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			System.out.println("\nKartenzahlung erfolgreich!");
			eingezahlterGesamtbetrag = zuZahlenderBetrag;

		} else {

			System.out.print("");
			System.out.print("Geld einwerfen (mindestens 5 Cent Münze, höchstens 200 Schein): ");

			System.out.println("Akzeptierte Münzen & Scheine: \n");

			System.out.println(
					"        *****           *****           *****           *****           *****           *****           *****           *****");
			System.out.println(
					"      *       *	      *       *	      *       *	      *       *	      *       *	      *       *	      *       *	      *       *");
			System.out.println(
					"     *    1    *     *    2    *     *    5    *     *    10   *     *    20   *     *    50   *     *    1    *     *    2    *");
			System.out.println(
					"      *  cent *	      *  cent *	      *  cent *	      *  cent *	      *  cent *	      *  cent *	      *  Euro *	      *  Euro *");
			System.out.println(
					"        *****           *****           *****           *****           *****           *****           *****           *****");
			System.out.println(
					" ---------------	 ---------------	 ---------------	 ---------------	 ---------------	 ---------------");
			System.out.println(
					"|               |	|               |	|               |	|               |	|               |	|               |");
			System.out.println(
					"|    5 Euro     |	|    10 Euro    |	|    20 Euro    |	|    50 Euro    |	|   100 Euro    |	|   200 Euro    |");
			System.out.println(
					"|               |	|               |	|               |	|               |	|               |	|               |");
			System.out.println(
					" ---------------	 ---------------	 ---------------	 ---------------	 ---------------	 ---------------");

			boolean a = true;

			while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
				ausstehenderBetrag = ausstehenderBetrag - eingezahlterGesamtbetrag;

				System.out.println("");
				if (a == true) {
					System.out.printf("Zu zahlen: %.2f €\n", ausstehenderBetrag);
					a = false;
				} else {
					System.out.printf("Noch zu zahlen: %.2f €\n", ausstehenderBetrag);
				}
				System.out.println("");

				System.out.print("Eingabe: ");
				double eingeworfenesGeld = tastatur.nextDouble();

				while (eingeworfenesGeld != 0.01 & eingeworfenesGeld != 0.02 & eingeworfenesGeld != 0.05
						& eingeworfenesGeld != 0.10 & eingeworfenesGeld != 0.20 & eingeworfenesGeld != 0.50
						& eingeworfenesGeld != 1 & eingeworfenesGeld != 2 & eingeworfenesGeld != 5
						& eingeworfenesGeld != 10 & eingeworfenesGeld != 20 & eingeworfenesGeld != 50
						& eingeworfenesGeld != 100 & eingeworfenesGeld != 200)

				{
					System.out.println("");
					System.out.println("Falsche Eingabe! Bitte wiederholen. \n");
					System.out.print("Eingabe: ");
					eingeworfenesGeld = tastatur.nextInt();

				}

				eingezahlterGesamtbetrag += eingeworfenesGeld;
			}
		}

		return eingezahlterGesamtbetrag;

	}

//	-----------------------------------------------------------------------------------------------------------
//	Fahrkarten ausgeben - METHODE:

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 15; i++) {
			System.out.print("=");
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("\n\n");

	}

//	-----------------------------------------------------------------------------------------------------------
//	Rückgeld ausgeben - METHODE:

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Sie erhalten einen Rückgabebetrag in Höhe von %.2f €\n \n", rückgabebetrag);

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 200.0) // 200 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|   200 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 200.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 100.0) // 100 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|   100 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 100.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 50.0) // 50 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    50 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 50.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 20.0) // 20 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    20 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 20.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 10.0) // 10 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|    10 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 10.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 5.0) // 5 EURO-Scheine
			{
				System.out.println(" ---------------");
				System.out.println("|               |");
				System.out.println("|     5 Euro    |");
				System.out.println("|               |");
				System.out.println(" ---------------");
				System.out.println("");

				rückgabebetrag -= 5.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    2    *");
				System.out.println("      *  Euro *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 2.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    1    *");
				System.out.println("      *  Euro *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 1.0;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    50   *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.5;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    20   *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.2;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    10   *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.1;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    5    *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.05;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.02)// 2 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    2    *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.02;
			}

			while (Math.round(rückgabebetrag * 100) / 100.0 >= 0.01)// 1 CENT-Münzen
			{
				System.out.println("        *****");
				System.out.println("      *       *");
				System.out.println("     *    1    *");
				System.out.println("      *  cent *");
				System.out.println("        *****");
				System.out.println("");

				rückgabebetrag -= 0.01;
			}

		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n");

		for (int i = 0; i < 1; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
