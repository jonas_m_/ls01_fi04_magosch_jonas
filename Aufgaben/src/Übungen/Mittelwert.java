package �bungen;

import java.util.Scanner;

public class Mittelwert {

    public static void main(String[] args) {
    
            
    double x = eingabeZahlAuslesen("Bitte geben Sie die erste Zahl ein.");
    double y = eingabeZahlAuslesen("Bitte geben Sie die zweite Zahl ein.");
    double m;
    
    m = berechneMittelwert (x,y);
    
    
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
    
    }

    public static double eingabeZahlAuslesen(String frage) {
        Scanner scan = new Scanner(System.in);
        System.out.println(frage);
        return scan.nextDouble();
            
    }
            
    public static double berechneMittelwert(double x, double y) {
        
        return (x + y) / 2.0;
    
       }
    
}